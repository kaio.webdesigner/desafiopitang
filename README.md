
## Desafio Pitang - API REST para Sistema de Avaliação


## 👨🏼‍💻 Funcionalidades

* #001. A API deve expor uma rota para cadastro de usuários;
* #002. A API deve expor uma rota para autenticação de usuários, utilizando e-mail e senha;
* #003. Um usuário autenticado poderá cadastrar um novo local;
* #004. Um usuário autenticado poderá visualizar os locais já cadastrados por ele;
* #005. Um usuário autenticado poderá avaliar um local, com rating (de 1 a 5) e comentário;
* #006. Um usuário autenticado poderá listar as avaliações de um determinado local;

## :bulb: Solução

Seguindo as funcionalidades solicitadas, a solução foi criar uma API REST onde existem três entidades principais (User, Local e Rating), ambas tem como objetivo englobar a regra de negocio passada pelo desafio, mais detalhes do que foi usado como tecnologia encontrasse mais abaixo no tópico **Recursos**.

## :tada: Recursos

* Utilizado **Java** ``v11.0.8``;
* Framework **Spring** ``v2.4.1``;
* **Spring Security** para validar as requisições;
* Servidor **Tomcat** embutido na aplicação;
* Processo de build via **Maven**;
* Banco de dados em memória para testes **H2**;
* Banco de dados Relacional **MySql** para aplicação;
* Persistência com **JPA/Hibernate** para mapear as entidades e fazer a comunicação com o banco;
* Testes unitários usando **Mockito**;
* **Exceptions**, exceção customizadas;
* Redução de código com **Lombok**;
* Foi usado **BCryptPasswordEncoder** do Spring para criptografar as senhas;
* Documentação dos Endpoints: https://documenter.getpostman.com/view/6258403/TVt184fx


OBS: Ao executar a aplicação para utilizar as rotas protegidas por (email e senha) podem ser acessadas utilizando as respectivas informções através do BASIC AUTH.


## :file_folder: Estrutura de arquivo
```
desafioPitang/
 │
 ├── src/main/java/
 │   └── com.kaioweb
 │       ├── config
 │       │   └── SecurityConfig.java
 │       │
 │       ├── controller
 │       │   ├── LocalController.java
 │       │   ├── RatingController.java
 │       │   └── UserController.java
 │       │
 │       ├── exception
 │       │   └── BadRequestEception.java
 │       │
 │       ├── model
 │       │   ├── Local.java
 │       │   ├── Rating.java
 │       │   ├── User.java
 │       │   └── dto
 │       │       ├── LocalDTO.java
 │       │       ├── RatingDTO.java
 │       │       └── UserDTO.java
 │       │
 │       ├── repository
 │       │   ├── LocalRepository.java
 │       │   ├── RatingRepository.java
 │       │   └── UserRepository.java
 │       │
 │       ├── service
 │       │   ├── LocalService.java
 │       │   ├── RatingService.java
 │       │   └── UserService.java
 │       │
 │       └── DesafioPitangApplication.java
 │
 ├── test/java/
 │   └── com.kaioweb 
 │       ├── controller
 │       │   ├── LocalControllerTest.java
 │       │   ├── RatingControllerTest.java
 │       │   └── UserControllerTest.java
 │       │        
 │       ├── repository
 │       │   ├── LocalRepositoryTest.java
 │       │   ├── RatingRepositoryTest.java
 │       │   └── UserRepositoryTest.java
 │       │
 │       ├── service
 │       │   ├── LocalServiceTest.java
 │       │   ├── RatingServiceTest.java
 │       │   └── UserServiceTest.java
 │       │
 │       └── DesafioPitangApplicationTest.java
 │
 ├── src/main/resources/
 │   └── application.yml
 │
 ├── .gitignore
 ├── mvnw/mvnw.cmd
 ├── pom.xml
 ├── README.md
 └── system.properties.md

```

## :copyright: Autorização
Sistema feito exclusivamente para o desafio da Pitang, desenvolvido por Kaio Henrique.