package com.kaioweb.controller;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.kaioweb.model.Local;
import com.kaioweb.model.Rating;
import com.kaioweb.model.User;
import com.kaioweb.model.dto.RatingDTO;
import com.kaioweb.service.LocalService;
import com.kaioweb.service.RatingService;
import com.kaioweb.service.UserService;
import com.kaioweb.util.ConvertRatingDTO;

import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("api")
@RequiredArgsConstructor
public class RatingController {
	
	private final LocalService localService;
	private final RatingService ratingService;
	private final UserService userService;
	
	@GetMapping(path = "locations/{id}/ratings")
	public ResponseEntity<Page<RatingDTO>> list(Pageable pageable, @PathVariable long id){
		Local local = localService.findById(id);
		return ResponseEntity.ok(ConvertRatingDTO.convertPageRatingToPageRatingDto(ratingService.listRatingsByLocal(pageable, local)));
	}
	
	@PostMapping(path = "{uid}/locations/{id}/ratings")
	public ResponseEntity<RatingDTO> save(@PathVariable long uid,@PathVariable long id,@RequestBody RatingDTO ratingDto){
		Local local = localService.findById(id);
		User user = userService.findById(uid);
		Rating rating = ConvertRatingDTO.convertRatingDtoToRating(ratingDto);
		rating.setLocal(local);
		rating.setUser(user);
		rating.setNameUserRating(user.getName());
		return ResponseEntity.ok(ConvertRatingDTO.convertRatingToRatingDto(ratingService.save(rating)));
	}
	
	
	
}
