package com.kaioweb.controller;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.kaioweb.model.Local;
import com.kaioweb.model.User;
import com.kaioweb.model.dto.LocalDTO;
import com.kaioweb.service.LocalService;
import com.kaioweb.service.UserService;
import com.kaioweb.util.ConvertLocalDTO;

import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("api")
@RequiredArgsConstructor
public class LocalController {
	private final LocalService localService;
	private final UserService userService;
	
	@GetMapping(path = "/locations")
	public ResponseEntity<Page<LocalDTO>> listAll(Pageable pageable){
		return ResponseEntity.ok(ConvertLocalDTO.convertPageLocalToPageLocalDto(localService.listAll(pageable)));		 
	}
	
	
	@PostMapping(path = "/users/{id}/locations")
	public ResponseEntity<LocalDTO> save(@PathVariable long id, @RequestBody LocalDTO localDto){
		User user = userService.findById(id);
		Local local = ConvertLocalDTO.convertLocalDtoToLocal(localDto);
		local.setUser(user);
		return ResponseEntity.ok(ConvertLocalDTO.convertLocalToLocalDto(localService.save(local)));
	}
	
	@GetMapping(path = "/users/{id}/locations")
	public ResponseEntity<List<LocalDTO>> listByUser(@PathVariable long id){
		User user = userService.findById(id);
		return ResponseEntity.ok(ConvertLocalDTO.convertListLocalToListLocalDto(localService.listMyLocations(user)));		 
	}
	


	

}
