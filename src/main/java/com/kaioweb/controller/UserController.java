package com.kaioweb.controller;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.kaioweb.model.dto.UserDTO;
import com.kaioweb.service.UserService;
import com.kaioweb.util.ConvertUserDTO;

import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("api/users")
@RequiredArgsConstructor
public class UserController {
	private final UserService userService;

	
	@GetMapping(path = "/")
	public ResponseEntity<List<UserDTO>> list(){
		return ResponseEntity.ok(ConvertUserDTO.convertListUserToListUserDto(userService.listAll()));
	}
	
	@GetMapping(path = "/{id}")
	public ResponseEntity<UserDTO> findById(@PathVariable long id){
		return ResponseEntity.ok(ConvertUserDTO.convertUserToUserDto(userService.findById(id)));
	}
	
	@PostMapping(path = "/new")
	public ResponseEntity<UserDTO> save(@RequestBody UserDTO userDto){
		return new ResponseEntity<>(
				ConvertUserDTO.convertUserToUserDto(
						userService.save(
								ConvertUserDTO.convertUserDtoToUser(userDto))), HttpStatus.CREATED);
	}
	
	@DeleteMapping(path = "/{id}")
	public ResponseEntity<Void> delete(@PathVariable long id){
		userService.delete(id);
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}
	
	@PutMapping(path = "/{id}")
	public ResponseEntity<Void> replace(@PathVariable long id, @RequestBody UserDTO userDto){
		userService.replace(id, ConvertUserDTO.convertUserDtoToUser(userDto));
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}


	
}
