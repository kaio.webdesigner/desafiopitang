package com.kaioweb.util;

import java.util.List;

import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.data.domain.Page;

import com.kaioweb.model.Rating;
import com.kaioweb.model.dto.RatingDTO;

public class ConvertRatingDTO {
	
	public static Page<RatingDTO> convertPageRatingToPageRatingDto(Page<Rating> list) {
		return new ModelMapper().map(list, new TypeToken<Page<RatingDTO>>() {}.getType());
	}
	
	public static List<RatingDTO> convertListRatingToListRatingDto(List<Rating> list) {
		return new ModelMapper().map(list, new TypeToken<List<RatingDTO>>() {}.getType());
	}
	
	public static Rating convertRatingDtoToRating(RatingDTO ratingDto) {
		return new ModelMapper().map(ratingDto, Rating.class);	
	}
	
	public static RatingDTO convertRatingToRatingDto(Rating rating) {
		return new ModelMapper().map(rating, RatingDTO.class);	
	}
	
}
