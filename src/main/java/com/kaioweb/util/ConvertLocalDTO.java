package com.kaioweb.util;

import java.util.List;

import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.data.domain.Page;

import com.kaioweb.model.Local;
import com.kaioweb.model.dto.LocalDTO;

public class ConvertLocalDTO {
	
	public static Page<LocalDTO> convertPageLocalToPageLocalDto(Page<Local> list) {
		return new ModelMapper().map(list, new TypeToken<Page<LocalDTO>>() {}.getType());
	}
	
	public static List<LocalDTO> convertListLocalToListLocalDto(List<Local> list) {
		return new ModelMapper().map(list, new TypeToken<List<LocalDTO>>() {}.getType());
	}

	public static LocalDTO convertLocalToLocalDto(Local local) {
		return new ModelMapper().map(local, LocalDTO.class);
	}
	
	public static Local convertLocalDtoToLocal(LocalDTO localDto) {
		return new ModelMapper().map(localDto, Local.class);	
	}
	
	
}
