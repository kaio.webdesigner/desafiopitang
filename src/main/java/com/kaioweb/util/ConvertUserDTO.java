package com.kaioweb.util;

import java.util.List;

import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;

import com.kaioweb.model.User;
import com.kaioweb.model.dto.UserDTO;

public class ConvertUserDTO {
	
	public static List<UserDTO> convertListUserToListUserDto(List<User> list) {
		return new ModelMapper().map(list, new TypeToken<List<UserDTO>>() {}.getType());
	}
	
	public static UserDTO convertUserToUserDto(User user){
		return new ModelMapper().map(user, UserDTO.class);	
	}
	
	public static User convertUserDtoToUser(UserDTO userDto){
		return new ModelMapper().map(userDto, User.class);
	}
	

}
