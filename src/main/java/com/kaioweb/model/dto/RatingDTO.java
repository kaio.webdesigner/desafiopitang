package com.kaioweb.model.dto;

import lombok.Data;

@Data
public class RatingDTO {
	
	private Long id;
	private int rating;
	private String comments;
	private String nameUserRating;
	

}
