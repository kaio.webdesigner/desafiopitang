package com.kaioweb.service;

import java.util.List;
import java.util.Optional;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.kaioweb.exception.BadRequestException;
import com.kaioweb.model.User;
import com.kaioweb.repository.UserRepository;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class UserService implements UserDetailsService{
	private final UserRepository userRepository;
		
	public List<User> listAll(){
		return userRepository.findAll();		
		
	}
	
	public User findById(long id){
		return userRepository.findById(id)
				.orElseThrow(() -> new BadRequestException("User not found"));
		}


	public User save(User user) {
		PasswordEncoder passwordEncoder = PasswordEncoderFactories.createDelegatingPasswordEncoder();
		user.setPassword(passwordEncoder.encode(user.getPassword()));
		return userRepository.save(user);		
	}

	public void delete(long id) {
		userRepository.delete(findById(id));
		
	}

	public void replace(long id, User user) {
		User savedUser = findById(id);
		user.setId(savedUser.getId());
		PasswordEncoder passwordEncoder = PasswordEncoderFactories.createDelegatingPasswordEncoder();
		user.setPassword(passwordEncoder.encode(user.getPassword()));
		userRepository.save(user);
		
	}

	@Override
	public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
		return Optional.ofNullable(userRepository.findByEmail(email))
				.orElseThrow(() -> new UsernameNotFoundException("User not found"));
	}
	
	
	
	
}
