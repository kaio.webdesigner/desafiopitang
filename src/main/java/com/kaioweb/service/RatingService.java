package com.kaioweb.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.kaioweb.model.Local;
import com.kaioweb.model.Rating;
import com.kaioweb.repository.RatingRepository;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class RatingService {
	private final RatingRepository ratingRepository;
	

	public Page<Rating> listRatingsByLocal(Pageable pageable, Local local){
		return ratingRepository.findAllByLocal(pageable, local);
	}
	
	public Rating save(Rating rating) {
		return ratingRepository.save(rating);
	}
	
}
