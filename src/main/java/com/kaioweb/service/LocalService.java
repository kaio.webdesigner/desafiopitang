package com.kaioweb.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import com.kaioweb.model.Local;
import com.kaioweb.model.User;
import com.kaioweb.repository.LocalRepository;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class LocalService {
	private final LocalRepository localRepository;
	
	public Page<Local> listAll(Pageable pageable){
		return localRepository.findAll(pageable);
	}
		
	public List<Local> listMyLocations(User user){
		return localRepository.findByUser(user);
	}
	
	public Local save(Local local) {
		return localRepository.save(local);
	}
	
	public Local findById(long id){
		return localRepository.findById(id)
				.orElseThrow(() -> new ResponseStatusException(HttpStatus.BAD_REQUEST, "Local not found"));		
	}
	
	

}
