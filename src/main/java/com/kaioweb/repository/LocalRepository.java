package com.kaioweb.repository;


import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.kaioweb.model.Local;
import com.kaioweb.model.User;

public interface LocalRepository extends JpaRepository<Local, Long>{
	
	List<Local> findByUser(User user);

}
