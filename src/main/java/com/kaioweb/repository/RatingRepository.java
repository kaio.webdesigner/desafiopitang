package com.kaioweb.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import com.kaioweb.model.Local;
import com.kaioweb.model.Rating;

public interface RatingRepository extends JpaRepository<Rating, Long>{

	Page<Rating> findAllByLocal(Pageable pageable, Local local);
}
