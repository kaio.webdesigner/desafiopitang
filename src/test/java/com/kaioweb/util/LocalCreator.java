package com.kaioweb.util;

import com.kaioweb.model.Local;
import com.kaioweb.model.User;
import com.kaioweb.model.dto.LocalDTO;

public class LocalCreator {
	
	public static Local createLocalToBeSaved() {
		User user = UserCreator.createUserToBeSaved();
		Local local = new Local();
		local.setName("test");
		local.setUser(user);
		return local;		
	}
	
	public static Local createValidLocal() {
		Local local = new Local();
		local.setId(1L);
		local.setName("test");
		local.setUser(UserCreator.createValidUser());
		return local;
	}
	
	public static LocalDTO createLocalDtoToBeSaved() {
		LocalDTO localDto = new LocalDTO();
		localDto.setName("test");
		return localDto;
	}
	
	public static LocalDTO createLocalDtoValid() {
		LocalDTO localDto = new LocalDTO();
		localDto.setId(1L);
		localDto.setName("test");
		return localDto;
	}
}
