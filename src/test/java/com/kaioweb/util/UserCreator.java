package com.kaioweb.util;

import com.kaioweb.model.User;
import com.kaioweb.model.dto.UserDTO;

public class UserCreator {
	
	public static User createUserToBeSaved() {
		User user = new User();
		user.setName("test");
		user.setEmail("test@gmail.com");
		user.setPassword("123");
		return user;		
	}

	
	public static User createValidUser() {
		User user = new User();
		user.setName("test");
		user.setId(1L);
		user.setEmail("test@gmail.com");
		user.setPassword("123");
		return user;
	}
	
	public static User createValidUpdateUser() {
		User user = new User();
		user.setName("test 2");
		user.setId(1L);
		user.setEmail("test@gmail.com");
		user.setPassword("123");
		return user;
	}
	
	public static UserDTO createUserDtoToBeSaved() {
		UserDTO userDto = new UserDTO();
		userDto.setName("test");
		userDto.setEmail("test@gmail.com");
		userDto.setPassword("123");
		return userDto;		
	}
	
	public static UserDTO createValidUserDto() {
		UserDTO userDto = new UserDTO();
		userDto.setId(1L);
		userDto.setName("test");
		userDto.setEmail("test@gmail.com");
		userDto.setPassword("123");
		return userDto;		
	}

}
