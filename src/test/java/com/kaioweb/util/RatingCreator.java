package com.kaioweb.util;

import com.kaioweb.model.Local;
import com.kaioweb.model.Rating;
import com.kaioweb.model.dto.RatingDTO;


public class RatingCreator {
	
	public static Rating createRatingToBeSaved() {
		Local local = LocalCreator.createLocalToBeSaved();
		Rating rating = new Rating();
		rating.setLocal(local);
		rating.setComments("test");
		rating.setRating(5);
		return rating;		
	}
	
	public static Rating createRatingValid() {
		Local local = LocalCreator.createLocalToBeSaved();
		Rating rating = new Rating();
		rating.setId(1L);
		rating.setLocal(local);
		rating.setComments("test");
		rating.setRating(5);
		return rating;		
	}
	
		
	public static RatingDTO createRatingDtoToBeSaved() {
		RatingDTO ratingDto = new RatingDTO();
		ratingDto.setComments("test");
		ratingDto.setRating(5);
		return ratingDto;
	}
}
