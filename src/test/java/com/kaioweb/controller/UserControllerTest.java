package com.kaioweb.controller;


import java.util.List;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.kaioweb.model.User;
import com.kaioweb.model.dto.UserDTO;
import com.kaioweb.service.UserService;
import com.kaioweb.util.UserCreator;


@ExtendWith(SpringExtension.class)
class UserControllerTest {
	
	@InjectMocks
	private UserController userController;
	
	@Mock
	private UserService userServiceMock;
	
	@BeforeEach
	void setUp() {
	
		
	BDDMockito.when(userServiceMock.listAll())
		.thenReturn(List.of(UserCreator.createUserToBeSaved()));
	
	BDDMockito.when(userServiceMock.findById(ArgumentMatchers.anyLong()))
		.thenReturn(UserCreator.createUserToBeSaved());
	
	BDDMockito.when(userServiceMock.save(ArgumentMatchers.any(User.class)))
						.thenReturn(UserCreator.createValidUser());
	
	BDDMockito.doNothing().when(userServiceMock).replace(ArgumentMatchers.anyLong(), ArgumentMatchers.any(User.class));
	
	BDDMockito.doNothing().when(userServiceMock).delete(ArgumentMatchers.anyLong());
	
	}
	
	@Test
	@DisplayName("List All users")
	void listAll_ReturnListUsers() {
		String expectedName = UserCreator.createUserToBeSaved().getName();
		List<UserDTO> usersList = userController.list().getBody();
		Assertions.assertThat(usersList).isNotNull()
										.isNotEmpty()
										.hasSize(1);
		Assertions.assertThat(usersList.get(0).getName()).isEqualTo(expectedName);
	}
	
	@Test
	@DisplayName("FindById return user")
	void findById_ReturnListUsers() {
		String expectedId = UserCreator.createUserToBeSaved().getName();
		UserDTO userDto = userController.findById(1).getBody();
		Assertions.assertThat(userDto).isNotNull();										
		Assertions.assertThat(userDto.getName()).isNotNull().isEqualTo(expectedId);
	}
	
	@Test
	@DisplayName("Save user")
	void saveUser_ReturnUserWithID() {
		UserDTO userDto = userController.save(UserCreator.createUserDtoToBeSaved()).getBody();
		Assertions.assertThat(userDto).isNotNull().isEqualTo(UserCreator.createValidUserDto());									
	}
	
	@Test
	@DisplayName("Update user")
	void updateUser() {
		Assertions.assertThatCode(()->userController.replace(1L, UserCreator.createUserDtoToBeSaved()))
				.doesNotThrowAnyException();
		
		ResponseEntity<Void> entity = userController.replace(1L, UserCreator.createUserDtoToBeSaved());
		Assertions.assertThat(entity.getStatusCode()).isEqualTo(HttpStatus.NO_CONTENT);
	
	}
	
	@Test
	@DisplayName("Delete user")
	void deleteUser() {
		
		Assertions.assertThatCode(() -> userController.delete(1)).doesNotThrowAnyException();
		
		ResponseEntity<Void> entity = userController.delete(1);
		Assertions.assertThat(entity).isNotNull();
		Assertions.assertThat(entity.getStatusCode()).isEqualTo(HttpStatus.NO_CONTENT);
	
	}

}
