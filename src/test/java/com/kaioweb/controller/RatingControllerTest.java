package com.kaioweb.controller;

import java.util.List;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.kaioweb.model.Local;
import com.kaioweb.model.Rating;
import com.kaioweb.model.User;
import com.kaioweb.model.dto.RatingDTO;
import com.kaioweb.service.LocalService;
import com.kaioweb.service.RatingService;
import com.kaioweb.service.UserService;
import com.kaioweb.util.LocalCreator;
import com.kaioweb.util.RatingCreator;
import com.kaioweb.util.UserCreator;


@ExtendWith(SpringExtension.class)
class RatingControllerTest {
	
	@InjectMocks
	private RatingController ratingController;
	
	@Mock
	private RatingService ratingService;
	
	@Mock
	private LocalService localServiceMock;
	
	@Mock
	private UserService userServiceMock;

	@Mock
	private ModelMapper modelMapper;
	
	@BeforeEach
	void setUp() {
		Page<Rating> ratingPage = new PageImpl<>(List.of(RatingCreator.createRatingToBeSaved()));
		BDDMockito.when(ratingService.listRatingsByLocal(ArgumentMatchers.any(), ArgumentMatchers.any(Local.class)))
			.thenReturn(ratingPage);
		
		BDDMockito.when(localServiceMock.findById(ArgumentMatchers.anyLong())).thenReturn(LocalCreator.createValidLocal());
		
		BDDMockito.when(ratingService.save(ArgumentMatchers.any(Rating.class)))
		.thenReturn(RatingCreator.createRatingValid());
		
		BDDMockito.when(userServiceMock.findById(ArgumentMatchers.anyLong())).thenReturn(UserCreator.createValidUser());
		
		
	}
	
	@Test
	@DisplayName("List Ratings By Local")
	void listAll_ReturnPageableLocal() {
		Page<RatingDTO> pageLocal = new ModelMapper().map(ratingController.list(null, 1L).getBody(), new TypeToken<Page<RatingDTO>>() {}.getType());
		Assertions.assertThat(pageLocal.toList()).isNotEmpty();
	}
	
	
	@Test
	@DisplayName("Save Rating by Local")
	void saveLocal_ReturnUserWithID() {
		User user = UserCreator.createValidUser();
		Local local = LocalCreator.createValidLocal();
		RatingDTO ratingDto = ratingController.save(user.getId(), local.getId(), RatingCreator.createRatingDtoToBeSaved()).getBody();
		Assertions.assertThat(ratingDto.getComments()).isEqualTo(RatingCreator.createRatingValid().getComments());
	}

}
