package com.kaioweb.controller;

import java.util.List;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.kaioweb.model.Local;
import com.kaioweb.model.User;
import com.kaioweb.model.dto.LocalDTO;
import com.kaioweb.service.LocalService;
import com.kaioweb.service.UserService;
import com.kaioweb.util.LocalCreator;
import com.kaioweb.util.UserCreator;


@ExtendWith(SpringExtension.class)
class LocalControllerTest {

	@InjectMocks
	private LocalController localController;
	
	@Mock
	private LocalService localServiceMock;
	@Mock
	private UserService userServiceMock;
	@Mock
	private ModelMapper modelMapper;
		
	@BeforeEach
	void setUp() {
		Page<Local> localPage = new PageImpl<>(List.of(LocalCreator.createValidLocal()));
		BDDMockito.when(localServiceMock.listAll(ArgumentMatchers.any()))
			.thenReturn(localPage);
		
		BDDMockito.when(userServiceMock.findById(ArgumentMatchers.anyLong())).thenReturn(UserCreator.createValidUser());
		
		BDDMockito.when(localServiceMock.save(ArgumentMatchers.any(Local.class)))
							.thenReturn(LocalCreator.createValidLocal());
		
		BDDMockito.when(localServiceMock.listMyLocations(ArgumentMatchers.any(User.class)))
					.thenReturn(List.of(LocalCreator.createValidLocal()));
		
		
	}
	
	@Test
	@DisplayName("List All Local")
	void listAll_ReturnPageableLocal() {
		Page<LocalDTO> pageLocal = new ModelMapper().map(localController.listAll(null).getBody(), new TypeToken<Page<LocalDTO>>() {}.getType());
		Assertions.assertThat(pageLocal.toList()).isNotEmpty();
	}
	
		
	@Test
	@DisplayName("Save Local")
	void saveLocal_ReturnUserWithID() {
		LocalDTO localDto = localController.save(1L, LocalCreator.createLocalDtoToBeSaved()).getBody();
		Assertions.assertThat(localDto.getId()).isNotNull();				
	}
	
	@Test
	@DisplayName("List All Local By User")
	void listAllByUser_ReturnList() {
		User user = UserCreator.createValidUser();
		List<LocalDTO> list_Local = new ModelMapper().map(localController.listByUser(user.getId()).getBody(), new TypeToken<List<LocalDTO>>() {}.getType());			
		Assertions.assertThat(list_Local).isNotEmpty();
		Assertions.assertThat(list_Local.get(0).getId()).isEqualTo(user.getId());
		
	}

}
