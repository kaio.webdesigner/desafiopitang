package com.kaioweb.repository;


import java.util.List;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import com.kaioweb.model.Local;
import com.kaioweb.model.User;
import com.kaioweb.util.LocalCreator;


@DataJpaTest
@DisplayName("Tests Local Repository")
class LocalRepositoryTest {

	@Autowired
	private LocalRepository localRepository;
	
	@Autowired
	private UserRepository userRepository;
	
	@Test
	@DisplayName("Save Local Repository")
	void saveLocal() {
		
		Local local = LocalCreator.createLocalToBeSaved();
		User userSaved = userRepository.save(local.getUser());
		local.setUser(userSaved);
		Local localSaved = localRepository.save(local);
		Assertions.assertThat(localSaved).isNotNull();
		Assertions.assertThat(localSaved.getId()).isNotNull();
		Assertions.assertThat(localSaved.getName()).isEqualTo(local.getName());		
	}
	
	@Test
	@DisplayName("FindAll Local Repository")
	void listLocal() {
		Local local = LocalCreator.createLocalToBeSaved();
		userRepository.save(local.getUser());
		Local localSaved = localRepository.save(local);
		List<Local> list_locations = localRepository.findAll();
		Assertions.assertThat(list_locations).isNotEmpty();
		Assertions.assertThat(list_locations).contains(localSaved);
		
	}
	
	@Test
	@DisplayName("FindByUser Local Repository")
	void findByUser() {
		Local local = LocalCreator.createLocalToBeSaved();
		userRepository.save(local.getUser());
		Local localSaved = localRepository.save(local);
		List<Local> locations_user = localRepository.findByUser(local.getUser());
		Assertions.assertThat(locations_user).isNotEmpty();
		Assertions.assertThat(locations_user).contains(localSaved);	
	}
	
	
	
}
