package com.kaioweb.repository;

import java.util.Optional;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import com.kaioweb.model.User;
import com.kaioweb.util.UserCreator;

@DataJpaTest
@DisplayName("Tests User Repository")
class UserRepositoryTest {
	
	@Autowired
	private UserRepository userRepository;
	
	@Test
	@DisplayName("Save User Repository")
	void saveUser() {
		User user = UserCreator.createUserToBeSaved();
		User userSaved = userRepository.save(user);
		Assertions.assertThat(userSaved).isNotNull();
		Assertions.assertThat(userSaved.getId()).isNotNull();
		Assertions.assertThat(userSaved.getName()).isEqualTo(user.getName());		
	}
	
	@Test
	@DisplayName("Update User Repository")
	void updateUser() {
		User user = UserCreator.createUserToBeSaved();
		User userSaved = userRepository.save(user);
		userSaved.setName("test2");	
		User userUpdated = userRepository.save(userSaved);
		Assertions.assertThat(userUpdated).isNotNull();
		Assertions.assertThat(userUpdated.getId()).isNotNull();
		Assertions.assertThat(userUpdated.getName()).isEqualTo(userSaved.getName());	
	}
	
	@Test
	@DisplayName("Delete User Repository")
	void deleteUser() {
		User user = UserCreator.createUserToBeSaved();
		User userSaved = userRepository.save(user);
		this.userRepository.delete(userSaved);
		Optional<User> userOptional = userRepository.findById(userSaved.getId());
		Assertions.assertThat(userOptional).isEmpty();
	}
	
	@Test
	@DisplayName("FindByEmail User Repository")
	void findByEmailUser() {
		User user = UserCreator.createUserToBeSaved();
		User userSaved = userRepository.save(user);
		String email = userSaved.getEmail();
		System.out.println(email);
		User users = userRepository.findByEmail(email);
		Assertions.assertThat(users.getEmail()).isEqualTo(email);
	}
	
	


}
