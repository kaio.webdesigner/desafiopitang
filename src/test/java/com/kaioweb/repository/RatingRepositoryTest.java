package com.kaioweb.repository;


import java.util.List;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import com.kaioweb.model.Local;
import com.kaioweb.model.Rating;
import com.kaioweb.model.User;
import com.kaioweb.util.RatingCreator;

@DataJpaTest
@DisplayName("Tests Rating Repository")
class RatingRepositoryTest {

	@Autowired
	private RatingRepository ratingRepository;
	
	@Autowired
	private LocalRepository localRepository;
	
	@Autowired
	private UserRepository userRepository;
	
	@Test
	@DisplayName("Save Rating Repository")
	void saveRating() {
		Rating rating = RatingCreator.createRatingToBeSaved();
		User userSaved = userRepository.save(rating.getLocal().getUser());
		rating.getLocal().setUser(userSaved);
		Local localSaved = localRepository.save(rating.getLocal());
		rating.setLocal(localSaved);
		Rating ratingSaved = ratingRepository.save(rating);
		Assertions.assertThat(ratingSaved).isNotNull();
		Assertions.assertThat(ratingSaved.getId()).isNotNull();
		Assertions.assertThat(ratingSaved.getComments()).isEqualTo(rating.getComments());		
		Assertions.assertThat(ratingSaved.getRating()).isEqualTo(rating.getRating());	
		Assertions.assertThat(ratingSaved.getLocal()).isEqualTo(rating.getLocal());	
	}
	
	@Test
	@DisplayName("FindAll Rating Repository")
	void listRating() {
		Rating rating = RatingCreator.createRatingToBeSaved();
		User userSaved = userRepository.save(rating.getLocal().getUser());
		rating.getLocal().setUser(userSaved);
		Local localSaved = localRepository.save(rating.getLocal());
		rating.setLocal(localSaved);
		Rating ratingSaved = ratingRepository.save(rating);
		List<Rating> list_ratings = ratingRepository.findAll();
		Assertions.assertThat(list_ratings).isNotEmpty();
		Assertions.assertThat(list_ratings).contains(ratingSaved);
		
	}
	
	@Test
	@DisplayName("FindByLocal Rating Repository")
	void findByLocal() {
		/*Rating rating = RatingCreator.createRatingToBeSaved();
		User userSaved = userRepository.save(rating.getLocal().getUser());
		rating.getLocal().setUser(userSaved);
		Local localSaved = localRepository.save(rating.getLocal());
		rating.setLocal(localSaved);
		Rating ratingSaved = ratingRepository.save(rating);
		List<Rating> ratings_local = ratingRepository.findAllByLocal(rating.getLocal());
		Assertions.assertThat(ratings_local).isNotEmpty();
		Assertions.assertThat(ratings_local).contains(ratingSaved);	*/
	}
	
	
	
}
