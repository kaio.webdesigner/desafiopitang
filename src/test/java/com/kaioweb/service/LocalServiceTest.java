package com.kaioweb.service;

import java.util.List;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.kaioweb.model.Local;
import com.kaioweb.model.User;
import com.kaioweb.repository.LocalRepository;
import com.kaioweb.repository.UserRepository;
import com.kaioweb.util.LocalCreator;
import com.kaioweb.util.UserCreator;

@ExtendWith(SpringExtension.class)
class LocalServiceTest {

	@InjectMocks
	private LocalService localService;
	
	@Mock
	private LocalRepository localRepositoryMock;
	@Mock
	private UserRepository userRepositoriryMock;
	
	@BeforeEach
	void setUp() {
	
		Page<Local> localPage = new PageImpl<>(List.of(LocalCreator.createValidLocal()));		
	BDDMockito.when(localRepositoryMock.findAll(ArgumentMatchers.any(PageRequest.class)))
		.thenReturn(localPage);
	
	BDDMockito.when(localRepositoryMock.save(ArgumentMatchers.any(Local.class)))
	.thenReturn(LocalCreator.createValidLocal());
	
	BDDMockito.when(localRepositoryMock.findByUser(ArgumentMatchers.any(User.class)))
	.thenReturn(List.of(LocalCreator.createValidLocal()));
	
	}

	@Test
	@DisplayName("List All Local")
	void listAll_ReturnPageableLocal() {
		Page<Local> pageLocal = localService.listAll(PageRequest.of(1,1));
		Assertions.assertThat(pageLocal).isNotEmpty();
	}
	
	@Test
	@DisplayName("Save Local")
	void saveLocal_ReturnUserWithID() {
		Local local = localService.save(LocalCreator.createLocalToBeSaved());
		Assertions.assertThat(local.getId()).isNotNull();				
	}
	
	@Test
	@DisplayName("List All Local By User")
	void listAllByUser_ReturnList() {
		User user = UserCreator.createValidUser();
		List<Local> list_Local = localService.listMyLocations(user);		
		Assertions.assertThat(list_Local).isNotEmpty();
		Assertions.assertThat(list_Local.get(0).getId()).isEqualTo(user.getId());
	}

}
