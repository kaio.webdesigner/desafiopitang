package com.kaioweb.service;


import java.util.List;
import java.util.Optional;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.kaioweb.model.Local;
import com.kaioweb.model.Rating;
import com.kaioweb.repository.LocalRepository;
import com.kaioweb.repository.RatingRepository;
import com.kaioweb.util.LocalCreator;
import com.kaioweb.util.RatingCreator;

@ExtendWith(SpringExtension.class)
class RatingServiceTest {

	@InjectMocks
	private RatingService ratingService;
	
	@Mock
	private RatingRepository ratingRepositoryMock;
	
	@Mock
	private LocalRepository localRepositoryMock;
	
	@BeforeEach
	void setUp() {
		Page<Rating> ratingPage = new PageImpl<>(List.of(RatingCreator.createRatingToBeSaved()));
		BDDMockito.when(ratingRepositoryMock.findAllByLocal(ArgumentMatchers.any(), ArgumentMatchers.any(Local.class)))
			.thenReturn(ratingPage);
		
		BDDMockito.when(ratingRepositoryMock.findById(ArgumentMatchers.anyLong())).thenReturn(Optional.of(RatingCreator.createRatingValid()));
		
		BDDMockito.when(ratingRepositoryMock.save(ArgumentMatchers.any(Rating.class)))
		.thenReturn(RatingCreator.createRatingValid());
		
		
	}
	
	@Test
	@DisplayName("List Ratings By Local")
	void listAll_ReturnPageableLocal() {
		Local local = LocalCreator.createValidLocal();
		Page<Rating> pageLocal = ratingService.listRatingsByLocal(null, local);
		Assertions.assertThat(pageLocal.toList()).isNotEmpty();
	}
	
	
	@Test
	@DisplayName("Save Rating by Local")
	void saveLocal_ReturnUserWithID() {
		Rating rating = ratingService.save(RatingCreator.createRatingToBeSaved());
		Assertions.assertThat(rating.getComments()).isEqualTo(RatingCreator.createRatingValid().getComments());
	}

}
