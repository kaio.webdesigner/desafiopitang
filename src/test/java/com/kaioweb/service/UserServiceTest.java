package com.kaioweb.service;


import java.util.List;
import java.util.Optional;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.kaioweb.model.User;
import com.kaioweb.repository.UserRepository;
import com.kaioweb.util.UserCreator;

@ExtendWith(SpringExtension.class)
class UserServiceTest {


	@InjectMocks
	private UserService userService;
	
	@Mock
	private UserRepository userRepositoryMock;
	
	@BeforeEach
	void setUp() {
	
		
	BDDMockito.when(userRepositoryMock.findAll())
		.thenReturn(List.of(UserCreator.createValidUser()));
	
	BDDMockito.when(userRepositoryMock.findById(ArgumentMatchers.anyLong()))
		.thenReturn(Optional.of(UserCreator.createValidUser()));
	
	BDDMockito.when(userRepositoryMock.save(ArgumentMatchers.any(User.class)))
						.thenReturn(UserCreator.createValidUser());
	
	
	BDDMockito.doNothing().when(userRepositoryMock).delete(ArgumentMatchers.any(User.class));
	
	}
	
	@Test
	@DisplayName("List All users")
	void listAll_ReturnListUsers() {
		String expectedName = UserCreator.createValidUser().getName();
		List<User> usersList = userService.listAll();
		Assertions.assertThat(usersList).isNotNull()
										.isNotEmpty()
										.hasSize(1);
		Assertions.assertThat(usersList.get(0).getName()).isEqualTo(expectedName);
	}
	
	@Test
	@DisplayName("FindById return user")
	void findById_ReturnListUsers() {
		String expectedId = UserCreator.createUserToBeSaved().getName();
		User user = userService.findById(1);
		Assertions.assertThat(user).isNotNull();										
		Assertions.assertThat(user.getName()).isNotNull().isEqualTo(expectedId);
	}
	
	@Test
	@DisplayName("Save user")
	void saveUser_ReturnUserWithID() {
		User user = userService.save(UserCreator.createUserToBeSaved());
		Assertions.assertThat(user).isNotNull();	
		Assertions.assertThat(user.getId()).isNotNull();
	}
	
	@Test
	@DisplayName("Update user")
	void updateUser() {
		Assertions.assertThatCode(()->userService.replace(1L, UserCreator.createUserToBeSaved()))
				.doesNotThrowAnyException();
	
	}
	
	@Test
	@DisplayName("Delete user")
	void deleteUser() {
		
		Assertions.assertThatCode(() -> userService.delete(1)).doesNotThrowAnyException();
	
	}
}
